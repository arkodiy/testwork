from prometheus_client import start_http_server, Gauge
import json
import logging
import random
import time
import clickhouse_connect


logging.basicConfig(
    level="INFO",
    format="%(asctime)s — %(name)s — %(levelname)s — %(message)s",
)
logger = logging.getLogger(__name__)

client = clickhouse_connect.get_client(host='clickhouse', port='8123', username='default', password='')

client.command("SET allow_experimental_object_type = 1;")
client.command("CREATE DATABASE IF NOT EXISTS test_work ENGINE = Memory COMMENT 'The temporary database';")
client.command("CREATE TABLE IF NOT EXISTS test_work.json ( o JSON ) ENGINE = Memory")

g = Gauge('my_ask01_bid_01_below_105', '1 if ask01 + bid_01 < 105')
g.set(0)

if __name__ == "__main__":

    start_http_server(8000)

    while True:
        msg = dict()
        for level in range(50):
            (
                msg[f"bid_{str(level).zfill(2)}"],
                msg[f"ask_{str(level).zfill(2)}"],
            ) = (
                random.randrange(1, 100),
                random.randrange(100, 200),
            )
        msg["stats"] = {
            "sum_bid": sum(v for k, v in msg.items() if "bid" in k),
            "sum_ask": sum(v for k, v in msg.items() if "ask" in k),
        }
        logger.info(f"{json.dumps(msg)}")
        
        client.command(f"INSERT INTO test_work.json VALUES ('{json.dumps(msg)}')")

        if int(msg["ask_01"]) + int(msg["bid_01"]) < 105:
            g.set(1)
        else:
            g.set(0)

        time.sleep(0.001)

